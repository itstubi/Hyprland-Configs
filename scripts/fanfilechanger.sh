#!/bin/bash

cd /home/tubi/.config/waybar/utils/ || { echo "Dizin açılamadı."; exit 1; }

file="fanvalue"

if [ ! -f "$file" ]; then
    echo "Dosya açılamadı."
    exit 1
fi

# dosya işlemleri
fan_value=$(cat "$file")
fan_value=$((fan_value + 1))

if [ "$fan_value" -eq 4 ]; then
    fan_value=1
fi

echo "$fan_value" > "$file"
