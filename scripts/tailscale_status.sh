#!/bin/sh

tailscale_output="$(tailscale status --json)"
active=$(jq --argjson j "$tailscale_output" -n '$j.Self.Online')
status=$(jq --argjson j "$tailscale_output" -n '$j.BackendState' -r)

if [ "$active" == 'true' ]; then
    TEXT=""
    CLASS="up"
else
    TEXT=""
    CLASS="down"
fi


printf '{"text": "%s", "class": "%s", "tooltip": "Tailsclae Status: %s"}\n' "$TEXT" "$CLASS" "$status"
