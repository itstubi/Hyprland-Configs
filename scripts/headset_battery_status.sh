#!/bin/sh

battery=$(upower -d | awk '/Device: \/org\/freedesktop\/UPower\/devices\/Your_Head_Set_Device/,/^$/' | grep 'percentage' | awk '{print $2}' | sed 's/%//')

device_name=$(upower -d | awk '/Device: \/org\/freedesktop\/UPower\/devices\/Your_Head_Set_Device/,/^$/' | grep 'model:' | awk -F ': ' '{print $2}' | sed 's/^[ \t]*//;s/[ \t]*$//')

if [ -z "$battery" ]; then
    CLASS="Disabled"
elif [ "$battery" -ge 90 ]; then
    CLASS="Full"
elif [ "$battery" -ge 20 ]; then
    CLASS="Good"
else
    CLASS="Critical"
fi

printf '{"class": "%s", "text": "%s%%", "tooltip": "Headset Device: %s"}\n' "$CLASS" "$battery" "$device_name"
