#!/bin/bash

cd /home/tubi/.config/waybar/utils/ || { echo '{"text": "Dizin açılamadı."}'; exit 1; }

file="fanvalue"

if [ ! -f "$file" ]; then
    echo '{"text": "Dosya açılamadı."}'
    exit 1
fi

fanvalue=$(cat "$file")

if [ "$fanvalue" -eq 1 ]; then
    TEXT="❄"
    CLASS="Quiet"
elif [ "$fanvalue" -eq 2 ]; then
    TEXT="⎋"
    CLASS="Balanced"
elif [ "$fanvalue" -eq 3 ]; then
    TEXT="⚔"
    CLASS="Performance"
fi

printf '{"text": "%s", "class": "%s", "tooltip": "Fan Mode Status: %s"}\n' "$TEXT" "$CLASS" "$CLASS"
