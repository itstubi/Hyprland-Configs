#!/bin/bash

cd /sys/bus/platform/devices/VPC2004:00/ || { echo "Dizin açılamadı."; exit 1; }

file="conservation_mode"

if [ ! -f "$file" ]; then
    echo "Dosya açılamadı."
    exit 1
fi

cons_mode=$(cat $"$file")

if [ "$cons_mode" -eq 1 ]; then
    TEXT=""
    CLASS="Activated"
elif [ "$cons_mode" -eq 0 ]; then
    TEXT=""
    CLASS="Disabled"
fi

printf '{"text": "%s", "class": "%s", "tooltip": "Conservation Mode %s"}\n' "$TEXT" "$CLASS" "$CLASS"
