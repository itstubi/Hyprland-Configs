MENU="rofi -dmenu"
CMD_POWEROFF="systemctl poweroff"
CMD_REBOOT="systemctl reboot"
CMD_SLEEP="systemctl suspend"
CMD_LOGOUT="loginctl kill-session self"

show_menu() {
    power="⏻ Power Off"
    reboot="⭮ Reboot"
    sleep="⏾ Sleep"
    logout="🔒 Logout"
    cancel="✖️ Cancel"

    options="$power\n$reboot\n$sleep\n$logout\n$cancel"

    echo $options
    chosen=$(echo -e $options | $MENU --prompt "Do you want to exit?")
    case $chosen in
        $power) $CMD_POWEROFF;;
        $reboot) $CMD_REBOOT;;
        $sleep) $CMD_SLEEP;;
        $logout) $CMD_LOGOUT;;
        *);;
    esac
}

show_menu
